package apps.jezior.rnote;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import apps.jezior.rnote.model.NoteDto;

public  class TestConnectionAsyncTask extends AsyncTask<Void, Void, NoteDto> {
    private ProgressDialog dialog;
    private MainActivity _mainActivity;

    public TestConnectionAsyncTask(MainActivity mainActivity)
    {
        _mainActivity = mainActivity;
    }

    @Override
    protected NoteDto doInBackground(Void... params) {
        try {
            final String url = _mainActivity.getString(R.string.rest_address) + _mainActivity.getString(R.string.test_method);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            NoteDto greeting = restTemplate.getForObject(url, NoteDto.class);
            return greeting;
        } catch (Exception e) {
            Log.e("TestConnectionAsyncTask", e.getMessage(), e);
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(_mainActivity);
        dialog.setMessage("Connecting to server...");
        dialog.show();

    }

    @Override
    protected void onPostExecute(NoteDto greeting) {
        super.onPostExecute(greeting);

        if(greeting == null)
        {
            _mainActivity.mStatusTextView.setText(_mainActivity.getString(R.string.connection_to_server_failed));
        }
        _mainActivity.mStatusTextView.setText(_mainActivity.getString(R.string.connection_to_server_ok));

        dialog.dismiss();
    }
}
