package apps.jezior.rnote;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import apps.jezior.rnote.model.NoteDto;


public class UpdateNoteAsyncTask extends AsyncTask<Void, Void, Void> {

    private NoteDto _noteDto;
    private EditNoteActivity _editNoteActivity;
    private ProgressDialog dialog;

    public UpdateNoteAsyncTask(NoteDto noteDto, EditNoteActivity editNoteActivity) {
        _noteDto = noteDto;

        _editNoteActivity = editNoteActivity;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            final String url = _editNoteActivity.getString(R.string.rest_address) + _editNoteActivity.getString(R.string.note_method) ;
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            restTemplate.postForObject(url, _noteDto, Void.class);

            Log.i("UpdateNoteAsyncTask", "Updated note for user : "  + _noteDto.getUserId());
        } catch (Exception e) {
            Log.e("UpdateNoteAsyncTask", e.getMessage(), e);
        }
        return null;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(_editNoteActivity);
        dialog.setMessage("Updating note...");
        dialog.show();
    }

    @Override
    protected void onPostExecute(Void greeting) {
        super.onPostExecute(greeting);

        dialog.dismiss();
        Intent intent = new Intent(_editNoteActivity, NotesListActivity.class);

        Bundle mBundle = new Bundle();
        mBundle.putParcelable(_editNoteActivity.getString(R.string.account_key), _editNoteActivity._account);
        intent.putExtras(mBundle);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        _editNoteActivity.startActivity(intent);
    }
}
