package apps.jezior.rnote;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;



import apps.jezior.rnote.model.NoteDto;

/**
 * Created by Jezior on 03.09.2016.
 */
public class AddNoteAsyncTask extends AsyncTask<Void, Void, Void> {

    private NoteDto _noteDto;
    private AddNoteActivity _addNoteActivity;
    private ProgressDialog dialog;

    public AddNoteAsyncTask(NoteDto noteDto, AddNoteActivity addNoteActivity) {
        _noteDto = noteDto;

        _addNoteActivity = addNoteActivity;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            final String url = _addNoteActivity.getString(R.string.rest_address) + _addNoteActivity.getString(R.string.note_method) ;
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            restTemplate.put(url, _noteDto);

            Log.i("GetNotesAsyncTask", "Saved note for user : "  + _noteDto.getUserId());
        } catch (Exception e) {
            Log.e("GetNotesAsyncTask", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(_addNoteActivity);
        dialog.setMessage("Adding note...");
        dialog.show();

    }

    @Override
    protected void onPostExecute(Void greeting) {
        super.onPostExecute(greeting);

        dialog.dismiss();
        Intent intent = new Intent(_addNoteActivity, NotesListActivity.class);

        Bundle mBundle = new Bundle();
        mBundle.putParcelable(_addNoteActivity.getString(R.string.account_key), _addNoteActivity._account);
        intent.putExtras(mBundle);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        _addNoteActivity.startActivity(intent);

    }
}
