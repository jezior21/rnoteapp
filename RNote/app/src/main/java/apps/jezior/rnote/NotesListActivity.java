package apps.jezior.rnote;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.util.Arrays;
import java.util.LinkedList;

import apps.jezior.rnote.model.NoteDto;

public class NotesListActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInAccount _account;
    private ListView _listView;
    private LinkedList<NoteDto> _listItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_list);
        initGoogleApiClient();
        initList();
        runRefresh();
    }

    private void initList()
    {
        _listView = (ListView)findViewById(R.id.list_view);
        _listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.i("NotesListActivity", "Clicked at position: "  + i);


                NoteDto currentItem = _listItems.get(i);
                Log.i("NotesListActivity", "About to edit note with id: "  + currentItem.getIdNote());

                runEditNote(currentItem);
            }


    });

    }

    private void runEditNote(NoteDto noteDto)
    {
        if (noteDto == null)
        {
            return;
        }

        Intent intent = new Intent(this, EditNoteActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle mBundle = new Bundle();
        mBundle.putParcelable(getString(R.string.account_key), _account);
        mBundle.putSerializable(getString(R.string.item_key), noteDto);
        intent.putExtras(mBundle);

        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    public void runRefresh()
    {
        new GetNotesAsyncTask(this, _account.getId()).execute();
    }

    public void DisplayNotes(NoteDto[] notes)
    {
        _listItems = new LinkedList<>();
        _listItems.addAll(Arrays.asList(notes));

        NoteDtoAdapter adapter = new NoteDtoAdapter(this, android.R.layout.activity_list_item);
        adapter.addAll(_listItems);
        _listView.setAdapter(adapter);

    }

    private void initGoogleApiClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        _account = (GoogleSignInAccount)getIntent().getExtras().get(getString(R.string.account_key));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.refresh_menu:
                doRefresh();
                break;
            case R.id.logout_menu:
                doLogout();
                break;
            case R.id.add_menu:
                doAdd();
                break;
        }
        return true;
    }

    private void doLogout()
    {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // ...
                    }
                });
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void doRefresh()
    {
        new GetNotesAsyncTask(this, _account.getId()).execute();
    }

    private void doAdd()
    {
        Intent intent = new Intent(this, AddNoteActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle mBundle = new Bundle();
        mBundle.putParcelable(getString(R.string.account_key), _account);
        intent.putExtras(mBundle);

        startActivity(intent);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
