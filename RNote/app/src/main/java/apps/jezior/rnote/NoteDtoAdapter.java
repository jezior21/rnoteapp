package apps.jezior.rnote;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.LinkedList;

import apps.jezior.rnote.model.NoteDto;

public class NoteDtoAdapter extends ArrayAdapter<NoteDto> {

    public NoteDtoAdapter(Context context, int resource) {
        super(context, resource);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NoteDto note = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_layout, parent, false);
        }

        TextView tvDate = (TextView) convertView.findViewById(R.id.itemDate);
        TextView tvText = (TextView) convertView.findViewById(R.id.itemText);
        tvDate.setText(android.text.format.DateFormat.format("yyyy-MM-dd hh:mm", note.getUpdatedOn()));

        String text = note.getNoteText();
        if(text.length() > 20)
        {
            text = text.substring(0, 15) + "...";
        }

        tvText.setText(text);

        return convertView;
    }
}
