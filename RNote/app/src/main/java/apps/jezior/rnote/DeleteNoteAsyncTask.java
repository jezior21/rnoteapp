package apps.jezior.rnote;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import apps.jezior.rnote.model.NoteDto;

public class DeleteNoteAsyncTask extends AsyncTask<Void, Void, Void> {

    private NoteDto _noteDto;
    private EditNoteActivity _editNoteActivity;
    private ProgressDialog dialog;

    public DeleteNoteAsyncTask(NoteDto noteDto, EditNoteActivity editNoteActivity) {
        _noteDto = noteDto;
        _editNoteActivity = editNoteActivity;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        JSONObject jsonBody;
        String requestBody;
        HttpURLConnection urlConnection = null;
        final String url = _editNoteActivity.getString(R.string.rest_address) + _editNoteActivity.getString(R.string.note_method) ;

        try {
            jsonBody = convertToJson(_noteDto);
            requestBody = Utils.buildPostParameters(jsonBody);
            urlConnection = (HttpURLConnection) Utils.makeRequest("DELETE", url, null, "application/json", requestBody);

            InputStream inputStream;
            // get stream
            if (urlConnection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
                inputStream = urlConnection.getInputStream();
            } else {
                inputStream = urlConnection.getErrorStream();
            }
            // parse stream
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String temp, response = "";
            while ((temp = bufferedReader.readLine()) != null) {
                response += temp;
            }
            Log.i("DeleteNoteAsyncTask", "Deleted note for user : "  + _noteDto.getUserId());
            Log.i("DeleteNoteAsyncTask", "Response : "  + response);
        } catch (JSONException | IOException e) {
            Log.e("DeleteNoteAsyncTask", e.getMessage(), e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(_editNoteActivity);
        dialog.setMessage("Deleting note...");
        dialog.show();
    }

    @Override
    protected void onPostExecute(Void greeting) {
        super.onPostExecute(greeting);

        dialog.dismiss();
        Intent intent = new Intent(_editNoteActivity, NotesListActivity.class);

        Bundle mBundle = new Bundle();
        mBundle.putParcelable(_editNoteActivity.getString(R.string.account_key), _editNoteActivity._account);
        intent.putExtras(mBundle);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        _editNoteActivity.startActivity(intent);
    }

    private JSONObject convertToJson(NoteDto note) throws JSONException {
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("idNote", note.getIdNote());
        jsonBody.put("userId", note.getUserId());
        jsonBody.put("createdOn", note.getCreatedOn().getTime());
        jsonBody.put("updatedOn", note.getUpdatedOn().getTime());
        jsonBody.put("noteText", note.getNoteText());
        jsonBody.put("flags", note.getFlags());

        return jsonBody;
    }
}
