package apps.jezior.rnote;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.Date;

import apps.jezior.rnote.model.NoteDto;

public class EditNoteActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;
    public GoogleSignInAccount _account;
    private NoteDto _currentItem;

    private EditText _noteEditText;
    private Button _saveButton;
    private Button _deleteButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);
        initGoogleApiClient();
        _currentItem = (NoteDto) getIntent().getExtras().get(getString(R.string.item_key));
        _noteEditText = (EditText)findViewById(R.id.note__edit_text_id);
        _saveButton = (Button)findViewById(R.id.button_edit_save_note);
        _deleteButton = (Button)findViewById(R.id.button_edit_delete_note);
        _noteEditText.setText(_currentItem.getNoteText());

        _saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveNote();
            }
        });
        _deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteNote();
            }
        });

    }

    private void saveNote() {
        _currentItem.setNoteText(_noteEditText.getText().toString());
        _currentItem.setUpdatedOn(new Date());
        new UpdateNoteAsyncTask(_currentItem, this).execute();
    }

    private void deleteNote() {
        new DeleteNoteAsyncTask(_currentItem, this).execute();
    }

    private void initGoogleApiClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this )
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        _account = (GoogleSignInAccount) getIntent().getExtras().get(getString(R.string.account_key));
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }
}
