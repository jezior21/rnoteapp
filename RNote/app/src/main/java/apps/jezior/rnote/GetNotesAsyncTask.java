package apps.jezior.rnote;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


import java.util.HashMap;
import java.util.Map;

import apps.jezior.rnote.model.NoteDto;


public class GetNotesAsyncTask extends AsyncTask<Void, Void, NoteDto[]> {

    private NotesListActivity _notesListActivity;
    private String _userId;
    private ProgressDialog dialog;

    public GetNotesAsyncTask(NotesListActivity notesListActivity, String userId)
    {
        _notesListActivity = notesListActivity;
        _userId = userId;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(_notesListActivity);
        dialog.setMessage("Loading notes...");
        dialog.show();
    }

    @Override
    protected void onPostExecute(NoteDto[] notes) {
        super.onPostExecute(notes);
        _notesListActivity.DisplayNotes(notes);
        dialog.dismiss();
    }

    @Override
    protected NoteDto[] doInBackground(Void... voids) {
        try {
            final String url = _notesListActivity.getString(R.string.rest_address) + _notesListActivity.getString(R.string.note_method) + "?userId={userId}";
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            Map<String, String> params = new HashMap<>();
            params.put("userId", _userId);

            ResponseEntity<NoteDto[]> response = restTemplate.getForEntity(url, NoteDto[].class, params);
            NoteDto[] result = response.getBody();
            Log.i("GetNotesAsyncTask", "Loaded notes: "  + result.length);
            return result;
        } catch (Exception e) {
            Log.e("GetNotesAsyncTask", e.getMessage(), e);
        }

        return null;
    }
}
